package com.mitchellbosecke.benchmark;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Martin Kouba
 */
public class ExpectedOutputTest {

    @BeforeClass
    public static void beforeClass() {
        Locale.setDefault(Locale.ENGLISH);
    }

    @Test
    public void testFreemarkerOutput() throws Exception {
        Freemarker freemarker = new Freemarker();
        freemarker.setup();
        assertOutput(freemarker.test());
    }

    @Test
    public void testRockerOutput() throws Exception {
        Rocker rocker = new Rocker();
        rocker.setup();
        assertOutput(rocker.test());
    }
//
//    @Test
//    public void testPebbleOutput() throws IOException, PebbleException {
//        Pebble pebble = new Pebble();
//        pebble.setup();
//        assertOutput(pebble.benchmark());
//    }
//
//    @Test
//    public void testVelocityOutput() throws IOException {
//        Velocity velocity = new Velocity();
//        velocity.setup();
//        assertOutput(velocity.benchmark());
//    }
//
//    @Test
//    public void testMustacheOutput() throws IOException {
//        Mustache mustache = new Mustache();
//        mustache.setup();
//        assertOutput(mustache.benchmark());
//    }
//
//    @Test
    public void testThymeleafOutput() throws Exception {
        Thymeleaf thymeleaf = new Thymeleaf();
        thymeleaf.setup();
        assertOutput(thymeleaf.test());
    }
//
//    @Test
//    public void testTrimouOutput() throws IOException {
//        Trimou trimou = new Trimou();
//        trimou.setup();
//        assertOutput(trimou.benchmark());
//    }
//
    @Test
    public void testHbsOutput() throws Exception {
        Handlebars hbs = new Handlebars();
        hbs.setup();
        assertOutput(hbs.test());
    }
    
    
    @Test
    public void testBeetl() throws Exception {
        Beetl beetl = new Beetl();
        beetl.setup();
        assertOutput(beetl.test());
    }

	@Test
	public void testJfinal() throws Exception {
		Enjoy jfinal = new Enjoy();
		jfinal.setup();
		String str = jfinal.test();
		assertOutput(str);
	}

    private void assertOutput(final String output) throws IOException {
        assertEquals(readExpectedOutputResource(), output.replaceAll("\\s", ""));
    }

    private String readExpectedOutputResource() throws IOException {
        StringBuilder builder = new StringBuilder();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(ExpectedOutputTest.class.getResourceAsStream("/expected-output.html")))) {
            for (;;) {
                String line = in.readLine();
                if (line == null) {
                  break;
                }
                builder.append(line);
            }
        }
        // Remove all whitespaces
        return builder.toString().replaceAll("\\s", "");
    }

}
